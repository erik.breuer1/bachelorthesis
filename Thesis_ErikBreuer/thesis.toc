\babel@toc {ngerman}{}
\contentsline {chapter}{Abkürzungsverzeichnis}{ii}{section*.2}%
\contentsline {chapter}{\numberline {1}Einleitung}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Motivation}{2}{section.1.1}%
\contentsline {chapter}{\numberline {2}Literaturüberblick}{4}{chapter.2}%
\contentsline {chapter}{\numberline {3}Modellierung}{10}{chapter.3}%
\contentsline {section}{\numberline {3.1}Problemdefinition}{10}{section.3.1}%
\contentsline {section}{\numberline {3.2}Distanzmatrix des Warenhaus-Layouts}{13}{section.3.2}%
\contentsline {chapter}{\numberline {4}Mathematische Formulierungen}{15}{chapter.4}%
\contentsline {section}{\numberline {4.1}Modellbeschreibung}{15}{section.4.1}%
\contentsline {section}{\numberline {4.2}Modell M\textsubscript {1}}{16}{section.4.2}%
\contentsline {section}{\numberline {4.3}Modell M\textsubscript {2}}{19}{section.4.3}%
\contentsline {section}{\numberline {4.4}Modell M\textsubscript {3}}{21}{section.4.4}%
\contentsline {chapter}{\numberline {5}Numerische Studien}{26}{chapter.5}%
\contentsline {section}{\numberline {5.1}Lösung von Modell M\textsubscript {\ref {model:M1}}}{26}{section.5.1}%
\contentsline {section}{\numberline {5.2}Lösung von Modell M\textsubscript {\ref {model:M2}}}{33}{section.5.2}%
\contentsline {section}{\numberline {5.3}Lösung von Modell M\textsubscript {\ref {model:M3}}}{33}{section.5.3}%
\contentsline {chapter}{\numberline {6}Zusammenfassung}{34}{chapter.6}%
