#BSUB -o /home/breuer/thesis/Code/Instances/JOB.%J.%I.table
#BSUB -q normal


cd /home/breuer/thesis/Code

./ batch.sh

module load python/3.7.3 gurobi/9.0.0 

python3 /home/breuer/thesis/Code/tables.py
