import random, json, copy

class Instance:

	euclidean = "euc"
	warehouse = "whs"

	# def __init__(self) -> None:
	# 	self._x = None
	# 	self._y = None

	def name(self):
		return self._name

	def distance(self, i, j):
		if self._distance_type == Instance.euclidean:
			return round(((self._x[str(i)] - self._x[str(j)])**2 + (self._y[str(i)] - self._y[str(j)])**2)**0.5, 0) # euclidean distance between nodes (rounded to integer)
		else:
			return round(abs(self._x[str(i)] - self._x[str(j)]) + abs(self._y[str(i)] - self._y[str(j)]), 0) # warehouse distance between nodes (manhattan metrik)

	def nodes(self, cluster = -1): # form clusters/orders for all nodes (incl. depot)
		if cluster == -1:
			return self._nodes
		else:
			return self._cluster_nodes_depot[cluster]

	def depot(self):
		return self._depot

	def clusters(self):
		return self._clusters

	def customers(self, cluster = -1): # form clusters/orders for customers
		if cluster == -1:
			return self._customers
		else:
			return self._cluster_nodes[cluster]

	def num_pickers(self):
		return self._num_pickers

	def x(self, i):
		return self._x[str(i)]

	def y(self, i):
		return self._y[str(i)]

	@staticmethod
	def generate(seed, n, k, mp, distance_type):

		inst = Instance()
		random.seed(seed)
		inst._name = str(n) + "-" + str(k) + "-" + str(mp) + "-" + distance_type + "-" + str(seed)
		inst._nodes = list(range(0, n + 1))	# all nodes (including depot)
		inst._customers = list(range(1, n + 1))	# all nodes (excluding depot)
		inst._clusters = list(range(0, k)) # clusters for all nodes (excluding depot)
		inst._depot = 0
		inst._cluster_nodes = [[] for i in range(k)] # initialize k empty clusters as list of lists for customers
		inst._cluster_nodes_depot = [[] for j in range(k)] # initialize k empty clusters as list of lists for nodes (incl depot)
		cluster_size = [0]*k
		cluster_size_depot = [0]*k
		inst._distance_type = distance_type

		for i in range(n): # increase cluster_size list length
			cluster_size[i%k] += 1
		i = 0
		for c in range(k):
			for j in range(cluster_size[c]):
				inst._cluster_nodes[c].append(inst._customers[i]) # create k clusters for all customers evenly
				i += 1

		for v in range(k): # create k clusters for all nodes 
			inst._cluster_nodes_depot[v] = copy.deepcopy(inst._cluster_nodes[v])
			inst._cluster_nodes_depot[v].insert(0, 0) 	# insert 0 in every cluster to include depot

		inst._x = {}
		inst._y = {}
		inst._num_pickers = mp

		inst._x[inst.depot()] = -5
		inst._y[inst.depot()] = -5

		for i in inst.customers(): # put customers in 100x100 grid
			inst._x[i] = random.randint(0, 100)
			inst._y[i] = random.randint(0, 100)

		return inst
		
	@staticmethod
	def serialize(self, path):
		with open(path, "w") as json_file:
			json.dump(self.__dict__, json_file, indent = 4)
		return json_file

	@staticmethod
	def deserialize(path):
		inst = Instance()
		with open(path) as file:
			inst.__dict__ = json.load(file)
		return inst

# def create_clusters(list, chunkSize = 3): # creates chunks of the given input (list) and outputs list of lists (split_list)
#     split_list = list()
#     numberChunks = len(list) // chunkSize + 1
#     for i in range(numberChunks):
#         listOflist.append(list[i*chunkSize:(i+1)*chunkSize])
#     return split_list
