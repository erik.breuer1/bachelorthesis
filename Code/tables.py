import sys, os
from os import listdir
from os.path import isfile, join

folder = "C:/Users/Erik/Documents/Uni/Bachelorarbeit/Code/results/M_1"
# keywords = ['Instanz Nr.', 'Best objective', 'best bound', 'gap']
keywords = ['Instanz Nr.', 'Zielfunktionswert', 'ZFGrenze', 'MIPLuecke', 'Laufzeit']
extracted_values = []
sol_files = [f for f in listdir(folder) if (isfile(join(folder, f)) and not f.endswith('.json'))] # list all non-.json files in folder

i = 0
for p in range(len(sol_files)):	# iterate over all non-.json files (output/solutions)
	with open(join(folder, sol_files[p]), "r+") as file:
		for line in file:				
			for k in keywords:
				if k in line:
					extracted_values.extend(line.split('=')[1:2]) # store single value after '=' symbol next to keywords in list extracted_values
					extracted_values[i] = str(extracted_values[i]).strip()
					if k == 'Instanz Nr.':
						print(str(round(float(extracted_values[i]), 0)) + ' &', end = '')
					elif k == 'Laufzeit':
						print(str(round(float(extracted_values[i]), 2)) + "\\\\")
					else:
						print(str(round(float(extracted_values[i]), 0)) + ' &', end = '')
					i += 1