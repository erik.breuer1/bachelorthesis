#BSUB -J "sagv[1-792]"
#BSUB -o /home/breuer/thesis/Code/results/JOB.%J.%I
#BSUB -q normal
#BSUB -x

cd /home/breuer/thesis/Code/Instances

module load python/3.7.3 gurobi/9.0.0 

python3 /home/breuer/thesis/Code/runner.py ${LSB_JOBINDEX} solveRouting
