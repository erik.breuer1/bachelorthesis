from gurobipy import *
from Solution import *

def solveVRP(inst):
	m = Model();

	x = {} # arcs
	t = {} # departure times
	f = {} # flow variables
	w = {} # time flow variables

	M = 500 # big M

	# departure time at each node
	t = m.addVar(vtype=GRB.CONTINUOUS, lb=0.0, ub=M, name="t")

	for i in inst.nodes():
		for j in inst.nodes():
			if i != j:
				# picker arcs between all nodes
				x[i, j] = m.addVar(vtype=GRB.BINARY, name="x_%s_%s" % (i, j))
				f[i, j] = m.addVar(vtype=GRB.CONTINUOUS, lb=0.0, name="f_%s_%s" % (i, j))
				w[i, j] = m.addVar(vtype=GRB.CONTINUOUS, lb=0.0, name="w_%s_%s" % (i, j))

	m.update()

	#if n <= 12: # add picker subtour elemination constraints for small instances
	#	for s in subsets(customers):
	#		m.addConstr(quicksum(xp[i, j] for i in s for j in s if i != j) <= len(s) - 1, "picker SEC " + str(s))

	m.addConstr(quicksum(x[inst.depot(), j] for j in inst.customers()) <= inst.num_pickers())

	for i in inst.customers():
		m.addConstr(quicksum(x[i, j] for j in inst.nodes() if i != j) == 1)
		m.addConstr(quicksum(x[j, i] for j in inst.nodes() if i != j) == 1)
		m.addConstr(quicksum(f[j, i] - f[i, j] for j in inst.nodes() if i != j) == 1)
		m.addConstr(w[inst.depot(), i] >= inst.distance(inst.depot(), i) * x[inst.depot(), i])
		m.addConstr(quicksum(w[i, j] - inst.distance(i, j) * x[i, j] for j in inst.nodes() if j != i) >= quicksum(w[j, i] for j in inst.nodes() if j != i))
		m.addConstr(t >= w[i, inst.depot()])

		for j in inst.nodes():
			if i != j:
				m.addConstr(x[i, j] + x[j, i] <= 1)

	for i in inst.nodes():
		for j in inst.nodes():
			if i != j:
				m.addConstr(f[i, j] <= x[i, j] * len(inst.nodes()))
				m.addConstr(w[i, j] <= x[i, j] * M)

	m._w = w
	m._x = x
	m._curr_m = M

	m.setObjective(t, GRB.MINIMIZE)

	def callback(m, where):
		if where == GRB.Callback.MIPSOL:
			new_m = round(m.cbGet(GRB.Callback.MIPSOL_OBJBST))
			nodeRel = m.cbGetNodeRel(model._vars)
			print(nodeRel)
			if new_m < m._curr_m:
				m._curr_m = new_m

				print("Set M to", m._curr_m)

				for (i, j) in m._x:
					m.cbLazy(m._w[i, j] <= m._x[i, j] * m._curr_m)

	m._vars = model.getVars()
	m.Params.LazyConstraints = 1
	m.update()
	m.optimize(callback)

	sol = Solution(inst)

	for i in inst.nodes():
		sol._departure[inst.depot()] = t.x

		for j in inst.nodes():
			if i != j and x[i, j].x >= 0.5:
				sol._prev_p[j] = i
				sol._next_p[i] = j

	return sol
