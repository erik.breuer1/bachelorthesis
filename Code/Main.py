import random, sys, itertools, json, os
from json import JSONEncoder
from input import Instance
import result as sol

def subsets(s): # yields all subsets of cardinality >= 2 of set s
	for n in range(2, len(s) + 1):
		yield itertools.combinations(s, n)

def print_solution(inst, sol, path):
	# print the tikz output in console:
	print("\\node[filled circle, label=south east:%.0f] (%d) at (%.2f, %.2f) {};" %(sol.departure(inst.depot()), 0, inst.x(inst.depot()) / 10.0, inst.y(inst.depot()) / 10.0))
	for i in inst.customers():
		print("\\node[base, label=south east:%.0f] (%d) at (%.2f, %.2f) {%d};" %(sol.departure(i), i, inst.x(i) / 10.0, inst.y(i) / 10.0, i))

	for i in inst.nodes():
		for j in inst.nodes():
			if sol.next_a(i) == j and sol.next_p(i) == j:
				print("\\draw[arc] (%d) to node [bend left = \\bendValue] {%.0f} (%d);" %(i, inst.distance(i, j), j))
				print("\\draw[arc, dashed] (%d) to[bend right = \\bendValue] node [black, midway, below] {%.0f} (%d);" %(i, inst.distance(i, j), j))
			elif sol.next_a(i) == j:
				print("\\draw[arc] (%d) to node [black, midway, below] {%.0f} (%d);" %(i, inst.distance(i, j), j))
			elif sol.next_p(i) == j:
				print("\\draw[arc, dashed] (%d) to node [black, midway, below] {%.0f} (%d);" %(i, inst.distance(i, j), j))


def print_tables(path):
	keywords = ['Instanz Nr.', 'Zielfunktionswert', 'ZFGrenze', 'MIPLuecke', 'Laufzeit']
	extracted_values = []
	i = 0
	with open(join(folder, sol_files[p]), "r+") as file:
		for line in file: 
			for k in keywords:
				if k in line:
					extracted_values.extend(line.split('=')[1:]) # store values after '=' symbol next to keywords in list extracted_values
					if k == 'Instanz Nr.':
						print(str(extracted_values[i]).replace('\n', '') + ' &', end = '')
					elif k == 'Laufzeit':
						print(str(extracted_values[i]).replace('\n', '') + "\\\\")
					else:
						print(str(extracted_values[i]).replace('\n', '') + ' &', end = '')
					i += 1


def generate_instances():
	i = 0
	for n in range(5, 16):					# range of nodes
		for k in range(1, min(n, 5)):		# range of clusters
			for mp in range(1, 4):			# range of pickers
				for dist in [Instance.euclidean, Instance.warehouse]:
					for seed in range(1, 3):	# range of random seed
						i += 1
						inst = Instance.generate(seed, n, k, mp, dist)
						path = os.path.dirname(os.path.realpath(__file__)) + '/Instances/' + inst.name() + '.json'
						Instance.serialize(inst, path)

#jobindex = 1
#if len(sys.argv) > 1:
#	jobindex = int(sys.argv[1])