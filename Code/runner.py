import sys, json, os
from os import listdir
from os.path import isfile, join
from main import generate_instances, print_solution, print_tables
from input import Instance
from grb_models import solveRouting, solveCompleteDiscrete

jobid = int(sys.argv[1]) - 1
algo  = (sys.argv[2])
generate_instances()
folder = "/home/breuer/thesis/Code/Instances/"
inst_files = [f for f in listdir(folder) if (isfile(join(folder, f)) and f.endswith('.json'))]	# list all .json files in folder

if algo == "solveRouting":												# solve Model M_1
	print('Model = M_{1}')													
	print("\nRun für die Instanz Nr. = " + str(jobid + 1))
	with open(join(folder, inst_files[jobid]), 'r') as json_file:
		inst = Instance.deserialize(join(folder, inst_files[jobid]))	# load .json file in inst
		sol = solveRouting(inst)										# load instance in Gurobi model and solve
		tikz_output = folder + "solution_M1_%d.txt" % (jobid)			# solution file for each .json
		print_solution(inst, sol, tikz_output)							# print tikz Output for solution to filepath

elif algo == "solveCompleteTimeFlow":									# solve Model M_2
	print('Model = 2')													
	print("\nRun für die Instanz Nr. = " + str(jobid + 1))
	with open(join(folder, inst_files[jobid]), 'r') as json_file:
		inst = Instance.deserialize(join(folder, inst_files[jobid]))	# load .json file in inst
		sol = solveCompleteTimeFlow(inst)								# load instance in Gurobi model and solve
		tikz_output = folder + "solution_M2_%d.txt" % (jobid)			# solution file for each .json
		print_solution(inst, sol, tikz_output)							# print tikz Output for solution to filepath

elif algo == "solveCompleteDiscrete":									# solve Model M_3
	print('Model = 3')
	print("\nRun für die Instanz Nr. = " + str(jobid + 1))
	with open(join(folder, inst_files[jobid]), 'r') as json_file:
		inst = Instance.deserialize(join(folder, inst_files[jobid]))	# load .json file in inst
		sol = solveCompleteDiscrete(inst)								# load instance in Gurobi model and solve
		tikz_output = folder + "solution_M3_%d.txt" % (jobid)			# solution file for each .json
		print_solution(inst, sol, tikz_output)							# print tikz Output for solution to filepath
		
