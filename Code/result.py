import input

class Solution:

	def __init__(self, inst):
		self._inst = inst
		n = len(inst.nodes())
		self._next_p = [None] * n
		self._next_a = [None] * n
		self._prev_p = [None] * n
		self._prev_a = [None] * n
		self._departure = [0] * n
		self._agv_routes = [None] * n
		self._agv_arcs = [None] * n
		self._start = [None] * n

	def agv_arcs(self):
		for r in self._agv_routes:
			i = self._start[r]
			while i != self._inst.depot():
				yield (i, self._next_a[i])

	def next_a(self, i):
		return self._next_a[i]

	def next_p(self, i):
		return self._next_p[i]

	def prev_a(self, i):
		return self._prev_a[i]

	def prev_p(self, i):
		return self._prev_p[i]

	def departure(self, i):
		return self._departure[i]

	def depot(self):
		return self._inst.depot()

	def distance(self, i, j):
		return self._inst.distance(i, j)

	def clusters(self):
		return self._inst.clusters()

	def customers(self, cluster = -1):
		return self._inst.customers(cluster)

	def x(self, i):
		return self._inst.x(i)

	def y(self, i):
		return self._inst.y(i)
