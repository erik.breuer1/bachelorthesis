from gurobipy import *
from result import Solution
from input import Instance
import main, math

def solveRouting(inst):

	m = Model("M_1")
	x = {} # AGV arcs
	p = {} # picker arcs
	t = {} # timestamps
	fp = {} # picker flow variables

	M = 5000 # big M

	for i in inst.nodes():
		# departure time at each node
		t[i] = m.addVar(vtype=GRB.CONTINUOUS, lb=0.0, ub=M, name="t_%s" % (i))

	for k in inst.clusters():
		for i in inst.customers(k):
			# AGV arcs from depot to all nodes in cluster and vice versa
			x[inst.depot(), i] = m.addVar(vtype=GRB.BINARY, name="x_%s_%s" % (inst.depot(), i))
			x[i, inst.depot()] = m.addVar(vtype=GRB.BINARY, name="x_%s_%s" % (i, inst.depot()))
			for j in inst.customers(k):
				if i != j:
					# AGV arcs between cluster nodes
					x[i, j] = m.addVar(vtype=GRB.BINARY, name="x_%s_%s" % (i, j))

	for i in inst.nodes():
		for j in inst.nodes():
			if i != j:
				# picker arcs between all nodes
				p[i, j] = m.addVar(vtype=GRB.BINARY, name="p_%s_%s" % (i, j))
				fp[i, j] = m.addVar(vtype=GRB.CONTINUOUS, lb=0.0, name="fp_%s_%s" % (i, j))

	m.update()

	for k in inst.clusters():
		m.addConstr(quicksum(x[inst.depot(), i] for i in inst.customers(k)) == 1, "AGV out degree depot " + str(k))
		m.addConstr(quicksum(x[i, inst.depot()] for i in inst.customers(k)) == 1, "AGV in degree depot " + str(k))

		# if len(inst.nodes()) <= 12: # add AGV subtour elemination constraints for small clusters
		# 	for s in main.subsets(inst.customers()):
		# 		m.addConstr(quicksum(x[i, j] for i in s for j in s if i != j) <= len(s) - 1, "AGV SEC " + str(s))

		for i in inst.customers(k):
			m.addConstr(t[i] >= inst.distance(inst.depot(), i) * x[inst.depot(), i], "AGV time coupling %s -> %s" % (inst.depot(), i))
			m.addConstr(quicksum(x[i, j] for j in inst.customers(k) if i != j) + x[i, inst.depot()] == 1, "AGV out degree " + str(i))
			m.addConstr(quicksum(x[j, i] for j in inst.customers(k) if i != j) + x[inst.depot(), i] == 1, "AGV in degree " + str(i))

			for j in inst.customers(k):
				if i != j:
					m.addConstr(t[j] >= t[i] + inst.distance(i, j) - M * (1 - x[i, j]), "AGV time coupling %s -> %s" % (i, j))
					m.addConstr(x[i, j] + x[j, i] <= 1)
					m.addConstr(x[i, j] + p[j, i] <= 1)

	m.addConstr(quicksum(p[inst.depot(), j] for j in inst.customers()) <= inst.num_pickers())

	# if len(inst.nodes()) <= 12: # add picker subtour elemination constraints for small instances
	# 	for s in main.subsets(inst.customers()):
	#  		m.addConstr(quicksum(p[i, j] for i in s for j in s if i != j) <= len(s) - 1, "picker SEC " + str(s))

	for i in inst.customers():
		m.addConstr(quicksum(p[i, j] for j in inst.nodes() if i != j) == 1)
		m.addConstr(quicksum(p[j, i] for j in inst.nodes() if i != j) == 1)
		m.addConstr(quicksum(fp[j, i] - fp[i, j] for j in inst.nodes() if i != j) == 1)
		m.addConstr(t[i] >= inst.distance(inst.depot(), i) * p[inst.depot(), i])

		for j in inst.nodes():
			if i != j:
				m.addConstr(t[j] >= t[i] + inst.distance(i, j) - M * (1 - p[i, j]))
				m.addConstr(p[i, j] + p[j, i] <= 1)

	# for i in inst.nodes():
	# 	for j in inst.nodes():
	# 		if i != j:
	# 			m.addConstr(fp[i, j] <= p[i, j] * n)

	m.setObjective(t[inst.depot()], GRB.MINIMIZE)

	#m.Params.MIPFocus = 1
	m.Params.timelimit = 3600.0

	m.update()
	m.optimize()

	print("Zielfunktionswert = " + str(round(m.objVal, 0)), "\nZFGrenze = " + str(round(m.ObjBound, 2)), "\nMIPLuecke = " + str(round(m.MIPGap, 2)), "\nLaufzeit = " + str(round(m.Runtime, 2))+ '\n')

	sol = Solution(inst)

	for i in inst.nodes():
		sol._departure[i] = t[i].x

		for j in inst.nodes():
			if i != j:
				if (i, j) in x and x[i, j].x >= 0.5:
					sol._prev_a[j] = i
					sol._next_a[i] = j
				if p[i, j].x >= 0.5:
					sol._prev_p[j] = i
					sol._next_p[i] = j

	return sol

def solveCompleteTimeFlow(inst):
	
	m = Model("M_2")
	x = {} # AGV arcs
	p = {} # picker arcs
	t = {} # timelengths flow
	T = {} # maximum time
	P = inst.num_pickers() # Maximum number of pickers

	M = 500 # big M

	for i in inst.nodes():
		for j in inst.nodes():
			if i != j:
				# time flow variables
				t[i, j] = m.addVar(vtype=GRB.CONTINUOUS, lb=0.0, ub=M, name='t_%s_%s' % (i, j))

	for k in inst.clusters():
		for i in inst.customers(k):
			# AGV arcs from depot to all nodes in cluster and vice versa
			x[inst.depot(), i] = m.addVar(vtype=GRB.BINARY, name="x_%s_%s" % (inst.depot(), i))
			x[i, inst.depot()] = m.addVar(vtype=GRB.BINARY, name="x_%s_%s" % (i, inst.depot()))
		for i in inst.nodes(k):
			for j in inst.nodes(k):
				if i != j:
					# AGV arcs between cluster nodes
					x[i, j] = m.addVar(vtype=GRB.BINARY, name="x_%s_%s" % (i, j))

	for i in inst.nodes():
		for j in inst.nodes():
			if i != j:
				# picker arcs between all nodes
				p[i, j] = m.addVar(vtype=GRB.BINARY, name="p_%s_%s" % (i, j))
				# fp[i, j] = m.addVar(vtype=GRB.CONTINUOUS, lb=0.0, name="fp_%s_%s" % (i, j))

	m.update()

	for k in inst.clusters():
		for i in inst.nodes(k):
			m.addConstr(quicksum((t[i, j] - inst.distance(i, j) * x[i, j]) for j in inst.nodes(k) if j != i) >= quicksum(t[j, i] for j in inst.customers() if j != i))
			m.addConstr(quicksum(x[j, i] for j in inst.nodes(k) if i != j) == 1)
		for j in inst.customers(k):
			m.addConstr(quicksum(x[i, j] for i in inst.nodes() if i != j) - quicksum(x[j, i] for i in inst.nodes() if i != j) == 0)
	for i in inst.customers():
		m.addConstr(quicksum(p[j, i] for j in inst.nodes() if i != j) == 1)
	for j in inst.customers():
		m.addConstr(quicksum(p[i, j] for i in inst.nodes() if i != j) - quicksum(p[j, i] for i in inst.nodes() if i != j) == 0)
	for k in inst.clusters():		
		for i in inst.nodes(k):
			for j in inst.nodes(k):
				if i != j: # upper and lower bound for time flow variables:
					m.addConstr(0 <= t[i, j]) 
					m.addConstr(t[i, j] <= (x[i, j] * M))


	m.addConstr(quicksum(p[inst.depot(), j] for j in inst.customers()) <= P, "don't exceed max number of Pickers")

	m.setObjective(t[j, inst.depot()], GRB.MINIMIZE)

	#m.Params.MIPFocus = 3
	m.setParam(GRB.Param.TimeLimit, 3600.0)

	m.update()
	m.optimize()

	print("Zielfunktionswert = " + str(m.objVal), "\nZFGrenze = " + str(m.objBound), "\nMIPLuecke = " + str(m.MIPGap), "\nLaufzeit = " + str(m.Runtime) + '\n')

	sol = Solution(inst)

	for i in inst.nodes():
		for j in inst.nodes():
			sol._departure[i] = t[i].x

		for j in inst.nodes():
			if i != j:
				if (i, j) in x and x[i, j].x >= 0.5:
					sol._prev_a[j] = i
					sol._next_a[i] = j
					if p[i, j].x >= 0.5:
						sol._prev_p[j] = i
						sol._next_p[i] = j
	return sol

def solveCompleteDiscrete(inst):

	m = Model("M_3")
	x = {} # AGV arcs
	p = {} # picker arcs
	P = inst.num_pickers() # cardinality of set of pickers
	makespan = {} # maximum time (to be minimized)
	tau = {} # additional time variable
	T = 500 # maximum time constant

	for k in inst.clusters():
		for i in inst.nodes(k):
			for j in inst.nodes(k):
				if i != j:
					for t in range(0, T + 1):
						if inst.distance(i, j) <= t:
							x[i, j, t] = m.addVar(vtype=GRB.BINARY, name="x_%s_%s_%s" % (i, j, t))	

	makespan = m.addVar(vtype=GRB.CONTINUOUS, lb=0.0, ub=T, name="makespan")

	for i in inst.nodes():
		for j in inst.nodes():
			if i != j:
				for t in range(0, T + 1):
					if inst.distance(i, j) <= t:
						p[i, j, t] = m.addVar(vtype=GRB.BINARY, name="p_%s_%s_%s" % (i, j, t))

	m.update()

	for j in inst.customers():
		m.addConstr(quicksum(p[i, j, t] for i in inst.nodes() if (i != j) for t in range(int(inst.distance(i, j)), T + 1)) == 1, "Picker in-degree " + str(j))
		m.addConstr(quicksum(p[j, i, t] for i in inst.nodes() if (i != j) for t in range(int(inst.distance(j, i)), T + 1)) == 1, "Picker out-degree "+ str(j))

			
	for k in inst.clusters():
		m.addConstr(quicksum(x[inst.depot(), i, t] for i in inst.customers(k) for t in range(0, T + 1)) == 1, "single AGV per order " + str(k))
		for j in inst.customers(k):
			m.addConstr(quicksum(x[i, j, t] for i in inst.nodes(k) if (i != j) for t in range(int(math.ceil(inst.distance(i, j))), T + 1)) == 1, "AGV in-degree " + str(j))
			m.addConstr(quicksum(x[j, i, t] for i in inst.nodes(k) if (i != j) for t in range(int(math.ceil(inst.distance(j, i))), T + 1)) == 1, "AGV out-degree " + str(j))
		for i in inst.customers(k):
			for t in range(0, T + 1):
				m.addConstr(quicksum(x[j, i, tau] for j in inst.nodes(k) if (i != j) for tau in range(0, t+1) if int(math.ceil(inst.distance(j, i))) <= tau)
				>= quicksum(x[i, j, t + int(math.ceil(inst.distance(i, j)))] for j in inst.nodes(k) if (i != j) if int(math.ceil(t + inst.distance(i, j))) <= T), "Departure AGV")

				m.addConstr(quicksum(p[j, i, tau] for j in inst.nodes() if (i != j) for tau in range(0, t+1) if int(math.ceil(inst.distance(j, i))) <= tau)
				>= quicksum(x[i, j, t + int(math.ceil(inst.distance(i, j)))] for j in inst.nodes(k) if (i != j) if int(math.ceil(t + inst.distance(i, j))) <= T), "Departure Picker after AGV")

				m.addConstr(quicksum(x[j, i, tau] for j in inst.nodes(k) if (i != j) for tau in range(0, t+1) if int(math.ceil(inst.distance(j, i))) <= tau)
				>= quicksum(p[i, j, t + int(math.ceil(inst.distance(i, j)))] for j in inst.nodes() if (i != j) if int(math.ceil(t + inst.distance(i, j))) <= T), "Departure AGV after Picker")

	for i in inst.customers():
		for t in range(0, T + 1):
			m.addConstr(quicksum(p[j, i, tau] for j in inst.nodes() if (i != j) for tau in range(0, t + 1) if int(math.ceil(inst.distance(j, i))) <= tau) 
			>= quicksum(p[i, j, t + int(math.ceil(inst.distance(i, j)))] for j in inst.nodes() if (i != j) if int(math.ceil(t + inst.distance(i, j))) <= T), "Departure Picker")

	for i in inst.customers():
		m.addConstr(quicksum(p[i, inst.depot(), t] * t for t in range(int(math.ceil(inst.distance(i, inst.depot()))), T + 1)) <= makespan, "link to obj")

	m.addConstr(quicksum(p[inst.depot(), j, t] for j in inst.customers() for t in range(int(math.ceil(inst.distance(inst.depot(), j))), T + 1)) <= P, "max picker count") # P as upper limit for picker-count

	# force latest 
	# for k in inst.clusters():
	#  	for i in inst.customers(k):
	#  		for t in range(0, T):
	#  			m.addConstr(quicksum(p[j, i, t] for j in inst.nodes() if i != j if (math.ceil(inst.distance(j, i)) <= t)) 
	#  			>= 2 * quicksum(p[i, j, t + inst.distance(i, j)] for j in inst.nodes() if i != j if (t + inst.distance(i, j)) <= T))
	#  			m.addConstr(quicksum(x[j, i, t] for j in inst.nodes(k) if i != j if (math.ceil(inst.distance(j, i)) <= t)) 
	#  			>= 2 * quicksum(x[i, j, tau] for j in inst.nodes(k) if i != j for tau in range(inst.distance(i, j), min(t - 1 + int(inst.distance(i, j), T)))))

	#m.Params.MIPFocus = 3
	m.Params.timeLimit = 3600

	m.setObjective(makespan, GRB.MINIMIZE)
	m.update()
	m.optimize()

	print("Zielfunktionswert = " + str(m.objVal), "\nZFGrenze = " + str(m.objBound), "\nMIPLuecke = " + str(m.MIPGap), "\nLaufzeit = " + str(m.Runtime) + '\n')

	sol = Solution(inst)

	# for v in m.getVars():
	# 	print(v.varName, v.x)

	for i in inst.nodes():
		sol._departure[i] = float("inf")
	for (i, j, t), var in x.items():
		if i != j:
			if var.x > 0.5:
				#  sol._departure[i] = min(sol._departure[i], t - inst.distance(i, j))
				sol._departure[i] = t - inst.distance(i, j)
				sol._prev_a[j] = i
				sol._next_a[i] = j
	for (i, j, t), var in p.items():
		if i != j:
			if var.x > 0.5:
				# sol._departure[i] = min(sol._departure[i], t - inst.distance(i, j))
				sol._departure[i] = t - inst.distance(i, j)
				sol._prev_p[j] = i
				sol._next_p[i] = j
		
	return sol
