# -*- coding: utf-8 -*-
"""
@author: Erik Breuer
"""
from gurobipy import *
import sys, random, json, InstGen


m = Model()
M = 500  # a big enough Integer
x = {}   # arcs for AGVs
f = {}   # flow variables for AGVs
xp = {}  # arcs for pickers
fp = {}  # flow variables for pickers
t = {}   # departure times

coords   = json.load('instances.json', 'r')
nodes    = json.load('sources.json', 'r')
targets  = json.load('targets.json', 'r')
orders   = json.load('orders.json', 'r')
depot    = InstGen.depot
#define the distances/travel times as the rounded euclidian distance:
c = lambda i, j : round(((coords[i][0] - coords[j][0])**2 + (coords[i][1] - coords[j][1])**2)**0.5, 2)

def subsets(s): # yields the subsets of nodeset s with a cardinality >= 2
    for n in range(2, len(s) + 1):
        yield from itertools.combinations(s, n)

for l in targets:
    for i in l:
        # AGV arcs linking depot and all nodes within orders
        x[depot,i] = m.addVar(vtype=GRB.BINARY, name='x_%s_%s' % (depot,i))
        x[i,depot] = m.addVar(vtype=GRB.BINARY, name='x_%s_%s' % (i,depot))
        for j in l:
            if i != j:
                # AGV arcs between all AGV_cluster nodes
                x[i,j] = m.addVar(vtype=GRB.BINARY, name='x_%s_%s' % (i,j))

for i in nodes:
    for j in nodes:
        if i != j:
            xp[i,j] = m.addVar(vtype=GRB.BINARY, name='xp_%s_%s' % (i,j))       # picker arcs between all nodes
            fp[i,j] = m.addVar(vtype=GRB.CONTINUOUS, name='fp_%s_%s' % (i,j))   # flow values on arcs

for i in nodes:
  t[i] = m.addVar(vtype=GRB.CONTINUOUS, name = 't_%s' % (i))    # departure times at node i

m.update()
m.setObjective(t[depot], GRB.MINIMIZE)

for i in targets:
   m.addConstr(quicksum(xp[i,j] for j in nodes if i != j) == 1)
   m.addConstr(quicksum(xp[j,i] for j in nodes if i != j) == 1)
   m.addConstr(quicksum(fp[j,i] - fp[i,j] for j in nodes if i != j) == 1)
   m.addConstr(t[i] >= c(depot,i) * xp[depot,i])
   for j in nodes:
       if i != j:
           m.addConstr(t[j] >= t[i] + c(i,j) - M * (1 - xp[i,j]))
           m.addConstr(xp[i,j] + xp[j,i] <= 1)
for i in nodes:
    for j in nodes:
        if i != j:
            m.addConstr(fp[i,j] <= xp[i,j] * InstGen.n)

for k, l in enumerate(orders):
    m.addConstr(quicksum(x[depot,i] for i in l) == 1, "AGV depot out-degree: %s" % k)
    m.addConstr(quicksum(x[i,depot] for i in l) == 1, 'AGV depot in-degree %s' % k)

    for i in l:
        m.addConstr(t[i] >= c[depot,i] * x[depot,i], 'Coupling AGV and time variables %s -> %s' % (i,j))
        m.addConstr(quicksum(x[i,j] for j in l if i != j) + x[i,depot] == 1, 'AGV out degree %s' % i)
        m.addConstr(quicksum(x[j,i] for j in l if i != j) + x[depot,i] == 1, 'AGV in degree %s' % i)
        for j in l:
            if j != i:
                m.addConstr(t[j] >= t[i] + c(i,j) - M * (1 - x[i,j]), "AGV time coupling %s -> %s" (i,j))
                m.addConstr(x[i,j] + x[j,i] <= 1)
                m.addConstr(x[i,j] + xp[j,i] <= 1)

m.addConstr(quicksum(xp[depot,j] for j in targets) <= InstGen.p)

# Commandline arguments trigger additional Subtour-Elimination Constraints:
if 'flow' in sys.argv:       # flow-based SECs
   for i in nodes:
       for j in nodes:
           m.addConstr(0 <= f[i,j] <= len(InstGen.n) * x[i,j])
           m.addConstr(quicksum(f[i,j] for i in nodes) - quicksum(f[i,j] for i in nodes) == 1)
elif 'graph' in sys.argv:   # graph-based SECs in a subset-formulation
    for s in subsets(targets):
        m.addConstr(quicksum(xp[i,j] for i in s for j in s if i != j) <= len(s) - 1, 'picker SEC %p' (s))
    for l in enumerate(orders):
        for s in subsets(l):
            m.addConstr(quicksum(x[i,j] for i in s for j in s if i != j) <= len(s) - 1, 'AGV SEC %p' (s))

m.update()
m.optimize()
